package com.portoseguro.bureau.mutifuncional.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.portoseguro.bureau.mutifuncional.domain.BaseMultifuncional} entity.
 */
public class BaseMultifuncionalDTO implements Serializable {

    private String id;

    @NotNull
    @Size(min = 3, max = 3)
    private String socioTorcedor;

    @NotNull
    @Size(min = 3, max = 3)
    private String asd;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSocioTorcedor() {
        return socioTorcedor;
    }

    public void setSocioTorcedor(String socioTorcedor) {
        this.socioTorcedor = socioTorcedor;
    }

    public String getAsd() {
        return asd;
    }

    public void setAsd(String asd) {
        this.asd = asd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BaseMultifuncionalDTO baseMultifuncionalDTO = (BaseMultifuncionalDTO) o;
        if (baseMultifuncionalDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), baseMultifuncionalDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BaseMultifuncionalDTO{" +
            "id=" + getId() +
            ", socioTorcedor='" + getSocioTorcedor() + "'" +
            ", asd='" + getAsd() + "'" +
            "}";
    }
}
