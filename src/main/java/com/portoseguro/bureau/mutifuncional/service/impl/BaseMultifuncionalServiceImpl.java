package com.portoseguro.bureau.mutifuncional.service.impl;

import com.portoseguro.bureau.mutifuncional.service.BaseMultifuncionalService;
import com.portoseguro.bureau.mutifuncional.domain.BaseMultifuncional;
import com.portoseguro.bureau.mutifuncional.repository.BaseMultifuncionalRepository;
import com.portoseguro.bureau.mutifuncional.service.dto.BaseMultifuncionalDTO;
import com.portoseguro.bureau.mutifuncional.service.mapper.BaseMultifuncionalMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BaseMultifuncional}.
 */
@Service
public class BaseMultifuncionalServiceImpl implements BaseMultifuncionalService {

    private final Logger log = LoggerFactory.getLogger(BaseMultifuncionalServiceImpl.class);

    private final BaseMultifuncionalRepository baseMultifuncionalRepository;

    private final BaseMultifuncionalMapper baseMultifuncionalMapper;

    public BaseMultifuncionalServiceImpl(BaseMultifuncionalRepository baseMultifuncionalRepository, BaseMultifuncionalMapper baseMultifuncionalMapper) {
        this.baseMultifuncionalRepository = baseMultifuncionalRepository;
        this.baseMultifuncionalMapper = baseMultifuncionalMapper;
    }

    /**
     * Save a baseMultifuncional.
     *
     * @param baseMultifuncionalDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public BaseMultifuncionalDTO save(BaseMultifuncionalDTO baseMultifuncionalDTO) {
        log.debug("Request to save BaseMultifuncional : {}", baseMultifuncionalDTO);
        BaseMultifuncional baseMultifuncional = baseMultifuncionalMapper.toEntity(baseMultifuncionalDTO);
        baseMultifuncional = baseMultifuncionalRepository.save(baseMultifuncional);
        return baseMultifuncionalMapper.toDto(baseMultifuncional);
    }

    /**
     * Get all the baseMultifuncionals.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    public Page<BaseMultifuncionalDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BaseMultifuncionals");
        return baseMultifuncionalRepository.findAll(pageable)
            .map(baseMultifuncionalMapper::toDto);
    }


    /**
     * Get one baseMultifuncional by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<BaseMultifuncionalDTO> findOne(String id) {
        log.debug("Request to get BaseMultifuncional : {}", id);
        return baseMultifuncionalRepository.findById(id)
            .map(baseMultifuncionalMapper::toDto);
    }

    /**
     * Delete the baseMultifuncional by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete BaseMultifuncional : {}", id);
        baseMultifuncionalRepository.deleteById(id);
    }
}
