package com.portoseguro.bureau.mutifuncional.service.mapper;

import com.portoseguro.bureau.mutifuncional.domain.*;
import com.portoseguro.bureau.mutifuncional.service.dto.BaseMultifuncionalDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BaseMultifuncional} and its DTO {@link BaseMultifuncionalDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BaseMultifuncionalMapper extends EntityMapper<BaseMultifuncionalDTO, BaseMultifuncional> {



    default BaseMultifuncional fromId(String id) {
        if (id == null) {
            return null;
        }
        BaseMultifuncional baseMultifuncional = new BaseMultifuncional();
        baseMultifuncional.setId(id);
        return baseMultifuncional;
    }
}
