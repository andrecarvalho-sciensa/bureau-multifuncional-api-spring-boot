package com.portoseguro.bureau.mutifuncional.service;

import com.portoseguro.bureau.mutifuncional.service.dto.BaseMultifuncionalDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.portoseguro.bureau.mutifuncional.domain.BaseMultifuncional}.
 */
public interface BaseMultifuncionalService {

    /**
     * Save a baseMultifuncional.
     *
     * @param baseMultifuncionalDTO the entity to save.
     * @return the persisted entity.
     */
    BaseMultifuncionalDTO save(BaseMultifuncionalDTO baseMultifuncionalDTO);

    /**
     * Get all the baseMultifuncionals.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BaseMultifuncionalDTO> findAll(Pageable pageable);


    /**
     * Get the "id" baseMultifuncional.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BaseMultifuncionalDTO> findOne(String id);

    /**
     * Delete the "id" baseMultifuncional.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
