package com.portoseguro.bureau.mutifuncional.domain;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A BaseMultifuncional.
 */
@Document(collection = "base_multifuncional")
public class BaseMultifuncional implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Size(min = 3, max = 3)
    @Field("socio_torcedor")
    private String socioTorcedor;

    @NotNull
    @Size(min = 3, max = 3)
    @Field("asd")
    private String asd;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSocioTorcedor() {
        return socioTorcedor;
    }

    public BaseMultifuncional socioTorcedor(String socioTorcedor) {
        this.socioTorcedor = socioTorcedor;
        return this;
    }

    public void setSocioTorcedor(String socioTorcedor) {
        this.socioTorcedor = socioTorcedor;
    }

    public String getAsd() {
        return asd;
    }

    public BaseMultifuncional asd(String asd) {
        this.asd = asd;
        return this;
    }

    public void setAsd(String asd) {
        this.asd = asd;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BaseMultifuncional)) {
            return false;
        }
        return id != null && id.equals(((BaseMultifuncional) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BaseMultifuncional{" +
            "id=" + getId() +
            ", socioTorcedor='" + getSocioTorcedor() + "'" +
            ", asd='" + getAsd() + "'" +
            "}";
    }
}
