package com.portoseguro.bureau.mutifuncional.repository;

import com.portoseguro.bureau.mutifuncional.domain.BaseMultifuncional;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the BaseMultifuncional entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BaseMultifuncionalRepository extends MongoRepository<BaseMultifuncional, String> {

}
