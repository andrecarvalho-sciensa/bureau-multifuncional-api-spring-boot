/**
 * View Models used by Spring MVC REST controllers.
 */
package com.portoseguro.bureau.mutifuncional.web.rest.vm;
