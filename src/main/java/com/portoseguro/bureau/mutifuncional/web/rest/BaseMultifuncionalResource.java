package com.portoseguro.bureau.mutifuncional.web.rest;

import com.portoseguro.bureau.mutifuncional.service.BaseMultifuncionalService;
import com.portoseguro.bureau.mutifuncional.web.rest.errors.BadRequestAlertException;
import com.portoseguro.bureau.mutifuncional.service.dto.BaseMultifuncionalDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.portoseguro.bureau.mutifuncional.domain.BaseMultifuncional}.
 */
@RestController
@RequestMapping("/api")
public class BaseMultifuncionalResource {

    private final Logger log = LoggerFactory.getLogger(BaseMultifuncionalResource.class);

    private static final String ENTITY_NAME = "multifuncionalBaseMultifuncional";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BaseMultifuncionalService baseMultifuncionalService;

    public BaseMultifuncionalResource(BaseMultifuncionalService baseMultifuncionalService) {
        this.baseMultifuncionalService = baseMultifuncionalService;
    }

    /**
     * {@code POST  /base-multifuncionals} : Create a new baseMultifuncional.
     *
     * @param baseMultifuncionalDTO the baseMultifuncionalDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new baseMultifuncionalDTO, or with status {@code 400 (Bad Request)} if the baseMultifuncional has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/base-multifuncionals")
    public ResponseEntity<BaseMultifuncionalDTO> createBaseMultifuncional(@Valid @RequestBody BaseMultifuncionalDTO baseMultifuncionalDTO) throws URISyntaxException {
        log.debug("REST request to save BaseMultifuncional : {}", baseMultifuncionalDTO);
        if (baseMultifuncionalDTO.getId() != null) {
            throw new BadRequestAlertException("A new baseMultifuncional cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BaseMultifuncionalDTO result = baseMultifuncionalService.save(baseMultifuncionalDTO);
        return ResponseEntity.created(new URI("/api/base-multifuncionals/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /base-multifuncionals} : Updates an existing baseMultifuncional.
     *
     * @param baseMultifuncionalDTO the baseMultifuncionalDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated baseMultifuncionalDTO,
     * or with status {@code 400 (Bad Request)} if the baseMultifuncionalDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the baseMultifuncionalDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/base-multifuncionals")
    public ResponseEntity<BaseMultifuncionalDTO> updateBaseMultifuncional(@Valid @RequestBody BaseMultifuncionalDTO baseMultifuncionalDTO) throws URISyntaxException {
        log.debug("REST request to update BaseMultifuncional : {}", baseMultifuncionalDTO);
        if (baseMultifuncionalDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BaseMultifuncionalDTO result = baseMultifuncionalService.save(baseMultifuncionalDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, baseMultifuncionalDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /base-multifuncionals} : get all the baseMultifuncionals.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of baseMultifuncionals in body.
     */
    @GetMapping("/base-multifuncionals")
    public ResponseEntity<List<BaseMultifuncionalDTO>> getAllBaseMultifuncionals(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of BaseMultifuncionals");
        Page<BaseMultifuncionalDTO> page = baseMultifuncionalService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /base-multifuncionals/:id} : get the "id" baseMultifuncional.
     *
     * @param id the id of the baseMultifuncionalDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the baseMultifuncionalDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/base-multifuncionals/{id}")
    public ResponseEntity<BaseMultifuncionalDTO> getBaseMultifuncional(@PathVariable String id) {
        log.debug("REST request to get BaseMultifuncional : {}", id);
        Optional<BaseMultifuncionalDTO> baseMultifuncionalDTO = baseMultifuncionalService.findOne(id);
        return ResponseUtil.wrapOrNotFound(baseMultifuncionalDTO);
    }

    /**
     * {@code DELETE  /base-multifuncionals/:id} : delete the "id" baseMultifuncional.
     *
     * @param id the id of the baseMultifuncionalDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/base-multifuncionals/{id}")
    public ResponseEntity<Void> deleteBaseMultifuncional(@PathVariable String id) {
        log.debug("REST request to delete BaseMultifuncional : {}", id);
        baseMultifuncionalService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
