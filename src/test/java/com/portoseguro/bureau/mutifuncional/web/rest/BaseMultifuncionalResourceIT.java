package com.portoseguro.bureau.mutifuncional.web.rest;

import com.portoseguro.bureau.mutifuncional.MultifuncionalApp;
import com.portoseguro.bureau.mutifuncional.domain.BaseMultifuncional;
import com.portoseguro.bureau.mutifuncional.repository.BaseMultifuncionalRepository;
import com.portoseguro.bureau.mutifuncional.service.BaseMultifuncionalService;
import com.portoseguro.bureau.mutifuncional.service.dto.BaseMultifuncionalDTO;
import com.portoseguro.bureau.mutifuncional.service.mapper.BaseMultifuncionalMapper;
import com.portoseguro.bureau.mutifuncional.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;


import java.util.List;

import static com.portoseguro.bureau.mutifuncional.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link BaseMultifuncionalResource} REST controller.
 */
@SpringBootTest(classes = MultifuncionalApp.class)
public class BaseMultifuncionalResourceIT {

    private static final String DEFAULT_SOCIO_TORCEDOR = "AAA";
    private static final String UPDATED_SOCIO_TORCEDOR = "BBB";

    private static final String DEFAULT_ASD = "AAA";
    private static final String UPDATED_ASD = "BBB";

    @Autowired
    private BaseMultifuncionalRepository baseMultifuncionalRepository;

    @Autowired
    private BaseMultifuncionalMapper baseMultifuncionalMapper;

    @Autowired
    private BaseMultifuncionalService baseMultifuncionalService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restBaseMultifuncionalMockMvc;

    private BaseMultifuncional baseMultifuncional;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BaseMultifuncionalResource baseMultifuncionalResource = new BaseMultifuncionalResource(baseMultifuncionalService);
        this.restBaseMultifuncionalMockMvc = MockMvcBuilders.standaloneSetup(baseMultifuncionalResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BaseMultifuncional createEntity() {
        BaseMultifuncional baseMultifuncional = new BaseMultifuncional()
            .socioTorcedor(DEFAULT_SOCIO_TORCEDOR)
            .asd(DEFAULT_ASD);
        return baseMultifuncional;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BaseMultifuncional createUpdatedEntity() {
        BaseMultifuncional baseMultifuncional = new BaseMultifuncional()
            .socioTorcedor(UPDATED_SOCIO_TORCEDOR)
            .asd(UPDATED_ASD);
        return baseMultifuncional;
    }

    @BeforeEach
    public void initTest() {
        baseMultifuncionalRepository.deleteAll();
        baseMultifuncional = createEntity();
    }

    @Test
    public void createBaseMultifuncional() throws Exception {
        int databaseSizeBeforeCreate = baseMultifuncionalRepository.findAll().size();

        // Create the BaseMultifuncional
        BaseMultifuncionalDTO baseMultifuncionalDTO = baseMultifuncionalMapper.toDto(baseMultifuncional);
        restBaseMultifuncionalMockMvc.perform(post("/api/base-multifuncionals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(baseMultifuncionalDTO)))
            .andExpect(status().isCreated());

        // Validate the BaseMultifuncional in the database
        List<BaseMultifuncional> baseMultifuncionalList = baseMultifuncionalRepository.findAll();
        assertThat(baseMultifuncionalList).hasSize(databaseSizeBeforeCreate + 1);
        BaseMultifuncional testBaseMultifuncional = baseMultifuncionalList.get(baseMultifuncionalList.size() - 1);
        assertThat(testBaseMultifuncional.getSocioTorcedor()).isEqualTo(DEFAULT_SOCIO_TORCEDOR);
        assertThat(testBaseMultifuncional.getAsd()).isEqualTo(DEFAULT_ASD);
    }

    @Test
    public void createBaseMultifuncionalWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = baseMultifuncionalRepository.findAll().size();

        // Create the BaseMultifuncional with an existing ID
        baseMultifuncional.setId("existing_id");
        BaseMultifuncionalDTO baseMultifuncionalDTO = baseMultifuncionalMapper.toDto(baseMultifuncional);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBaseMultifuncionalMockMvc.perform(post("/api/base-multifuncionals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(baseMultifuncionalDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BaseMultifuncional in the database
        List<BaseMultifuncional> baseMultifuncionalList = baseMultifuncionalRepository.findAll();
        assertThat(baseMultifuncionalList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void checkSocioTorcedorIsRequired() throws Exception {
        int databaseSizeBeforeTest = baseMultifuncionalRepository.findAll().size();
        // set the field null
        baseMultifuncional.setSocioTorcedor(null);

        // Create the BaseMultifuncional, which fails.
        BaseMultifuncionalDTO baseMultifuncionalDTO = baseMultifuncionalMapper.toDto(baseMultifuncional);

        restBaseMultifuncionalMockMvc.perform(post("/api/base-multifuncionals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(baseMultifuncionalDTO)))
            .andExpect(status().isBadRequest());

        List<BaseMultifuncional> baseMultifuncionalList = baseMultifuncionalRepository.findAll();
        assertThat(baseMultifuncionalList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkAsdIsRequired() throws Exception {
        int databaseSizeBeforeTest = baseMultifuncionalRepository.findAll().size();
        // set the field null
        baseMultifuncional.setAsd(null);

        // Create the BaseMultifuncional, which fails.
        BaseMultifuncionalDTO baseMultifuncionalDTO = baseMultifuncionalMapper.toDto(baseMultifuncional);

        restBaseMultifuncionalMockMvc.perform(post("/api/base-multifuncionals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(baseMultifuncionalDTO)))
            .andExpect(status().isBadRequest());

        List<BaseMultifuncional> baseMultifuncionalList = baseMultifuncionalRepository.findAll();
        assertThat(baseMultifuncionalList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllBaseMultifuncionals() throws Exception {
        // Initialize the database
        baseMultifuncionalRepository.save(baseMultifuncional);

        // Get all the baseMultifuncionalList
        restBaseMultifuncionalMockMvc.perform(get("/api/base-multifuncionals?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(baseMultifuncional.getId())))
            .andExpect(jsonPath("$.[*].socioTorcedor").value(hasItem(DEFAULT_SOCIO_TORCEDOR.toString())))
            .andExpect(jsonPath("$.[*].asd").value(hasItem(DEFAULT_ASD.toString())));
    }
    
    @Test
    public void getBaseMultifuncional() throws Exception {
        // Initialize the database
        baseMultifuncionalRepository.save(baseMultifuncional);

        // Get the baseMultifuncional
        restBaseMultifuncionalMockMvc.perform(get("/api/base-multifuncionals/{id}", baseMultifuncional.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(baseMultifuncional.getId()))
            .andExpect(jsonPath("$.socioTorcedor").value(DEFAULT_SOCIO_TORCEDOR.toString()))
            .andExpect(jsonPath("$.asd").value(DEFAULT_ASD.toString()));
    }

    @Test
    public void getNonExistingBaseMultifuncional() throws Exception {
        // Get the baseMultifuncional
        restBaseMultifuncionalMockMvc.perform(get("/api/base-multifuncionals/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateBaseMultifuncional() throws Exception {
        // Initialize the database
        baseMultifuncionalRepository.save(baseMultifuncional);

        int databaseSizeBeforeUpdate = baseMultifuncionalRepository.findAll().size();

        // Update the baseMultifuncional
        BaseMultifuncional updatedBaseMultifuncional = baseMultifuncionalRepository.findById(baseMultifuncional.getId()).get();
        updatedBaseMultifuncional
            .socioTorcedor(UPDATED_SOCIO_TORCEDOR)
            .asd(UPDATED_ASD);
        BaseMultifuncionalDTO baseMultifuncionalDTO = baseMultifuncionalMapper.toDto(updatedBaseMultifuncional);

        restBaseMultifuncionalMockMvc.perform(put("/api/base-multifuncionals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(baseMultifuncionalDTO)))
            .andExpect(status().isOk());

        // Validate the BaseMultifuncional in the database
        List<BaseMultifuncional> baseMultifuncionalList = baseMultifuncionalRepository.findAll();
        assertThat(baseMultifuncionalList).hasSize(databaseSizeBeforeUpdate);
        BaseMultifuncional testBaseMultifuncional = baseMultifuncionalList.get(baseMultifuncionalList.size() - 1);
        assertThat(testBaseMultifuncional.getSocioTorcedor()).isEqualTo(UPDATED_SOCIO_TORCEDOR);
        assertThat(testBaseMultifuncional.getAsd()).isEqualTo(UPDATED_ASD);
    }

    @Test
    public void updateNonExistingBaseMultifuncional() throws Exception {
        int databaseSizeBeforeUpdate = baseMultifuncionalRepository.findAll().size();

        // Create the BaseMultifuncional
        BaseMultifuncionalDTO baseMultifuncionalDTO = baseMultifuncionalMapper.toDto(baseMultifuncional);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBaseMultifuncionalMockMvc.perform(put("/api/base-multifuncionals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(baseMultifuncionalDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BaseMultifuncional in the database
        List<BaseMultifuncional> baseMultifuncionalList = baseMultifuncionalRepository.findAll();
        assertThat(baseMultifuncionalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteBaseMultifuncional() throws Exception {
        // Initialize the database
        baseMultifuncionalRepository.save(baseMultifuncional);

        int databaseSizeBeforeDelete = baseMultifuncionalRepository.findAll().size();

        // Delete the baseMultifuncional
        restBaseMultifuncionalMockMvc.perform(delete("/api/base-multifuncionals/{id}", baseMultifuncional.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<BaseMultifuncional> baseMultifuncionalList = baseMultifuncionalRepository.findAll();
        assertThat(baseMultifuncionalList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BaseMultifuncional.class);
        BaseMultifuncional baseMultifuncional1 = new BaseMultifuncional();
        baseMultifuncional1.setId("id1");
        BaseMultifuncional baseMultifuncional2 = new BaseMultifuncional();
        baseMultifuncional2.setId(baseMultifuncional1.getId());
        assertThat(baseMultifuncional1).isEqualTo(baseMultifuncional2);
        baseMultifuncional2.setId("id2");
        assertThat(baseMultifuncional1).isNotEqualTo(baseMultifuncional2);
        baseMultifuncional1.setId(null);
        assertThat(baseMultifuncional1).isNotEqualTo(baseMultifuncional2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BaseMultifuncionalDTO.class);
        BaseMultifuncionalDTO baseMultifuncionalDTO1 = new BaseMultifuncionalDTO();
        baseMultifuncionalDTO1.setId("id1");
        BaseMultifuncionalDTO baseMultifuncionalDTO2 = new BaseMultifuncionalDTO();
        assertThat(baseMultifuncionalDTO1).isNotEqualTo(baseMultifuncionalDTO2);
        baseMultifuncionalDTO2.setId(baseMultifuncionalDTO1.getId());
        assertThat(baseMultifuncionalDTO1).isEqualTo(baseMultifuncionalDTO2);
        baseMultifuncionalDTO2.setId("id2");
        assertThat(baseMultifuncionalDTO1).isNotEqualTo(baseMultifuncionalDTO2);
        baseMultifuncionalDTO1.setId(null);
        assertThat(baseMultifuncionalDTO1).isNotEqualTo(baseMultifuncionalDTO2);
    }
}
